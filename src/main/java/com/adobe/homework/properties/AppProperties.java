package com.adobe.homework.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class AppProperties {
	private static final Logger LOG = Logger.getLogger(AppProperties.class);
	private static final String fileName = "application.properties";
	private static Integer serverPort;
	private static Integer noWorkers;
	private static String baseDirectory;

	private AppProperties() {
		// hide constructor
	}

	public static Integer getServerPort() {
		return serverPort;
	}

	public static Integer getNoWorkers() {
		return noWorkers;
	}

	public static String getBaseDirectory() {
		return baseDirectory;
	}

	/**
	 * Reads properties from config file
	 */
	public static void loadProperties() {
		LOG.info(String.format("reading %s config file.", fileName));
		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = AppProperties.class.getClassLoader().getResourceAsStream(fileName);
			if (null == input) {
				LOG.error(String.format("Failed to load %s file", fileName));
				return;
			}
			prop.load(input);
			loadProperties(prop);
		} catch (Exception e) {
			LOG.error(String.format("Failed to load %s file. {%s", fileName, e));
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					LOG.error(String.format("Failed to close %s file. %s", fileName, e));
				}
			}
		}
	}

	private static void loadProperties(Properties prop) {
		serverPort = Integer.valueOf(prop.getProperty("server.port"));
		LOG.info(String.format("Setting server listening port = %s", serverPort));

		noWorkers = Integer.valueOf(prop.getProperty("server.workers.number"));
		LOG.info(String.format("Setting total number of workers = %s", noWorkers));

		baseDirectory = prop.getProperty("server.base.directory");
		LOG.info(String.format("Setting base directory = %s", baseDirectory));
	}
}
