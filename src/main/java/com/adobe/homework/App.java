package com.adobe.homework;

import com.adobe.homework.properties.AppProperties;
import com.adobe.homework.webserver.Component;
import com.adobe.homework.webserver.SimpleHttpServer;

public class App {

	public static Component httpServer;

	/**
	 * Application start
	 */
	public static void main(String[] args) {
		loadProperties();
		startWebServer();
		addShutdownHook();
		sleepForever();
	}

	/**
	 * Load all config properties files into memory
	 */
	private static void loadProperties() {
		AppProperties.loadProperties();
	}

	/**
	 * Start the web server into a separate thread
	 */
	private static void startWebServer() {
		new Thread() {
			@Override
			public void run() {
				httpServer = new SimpleHttpServer();
				httpServer.Start();
			}
		}.start();
	}

	/**
	 * Ensure that web server will be properly shutdown
	 */
	private static void addShutdownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				httpServer.Stop();
			}
		});
	}

	/**
	 * Main thread sleeps forever after finish its job
	 */
	private static void sleepForever() {
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				throw new RuntimeException("DING DING", e);
			}
		}
	}
}
