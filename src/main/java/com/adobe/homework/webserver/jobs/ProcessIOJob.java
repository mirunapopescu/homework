package com.adobe.homework.webserver.jobs;

import static com.adobe.homework.webserver.utils.Constants.NEW_LINE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.adobe.homework.webserver.entities.Request;
import com.adobe.homework.webserver.entities.Response;
import com.adobe.homework.webserver.exceptions.HttpException;
import com.adobe.homework.webserver.utils.HttpConverter;

public class ProcessIOJob implements Callable<Object> {
	private static final Logger LOG = Logger.getLogger(ProcessIOJob.class);
	private Socket clientSocket;
	private RequestProcessor processor;
	private BufferedReader in;
	private PrintWriter out;

	public ProcessIOJob(Socket clientSocket) {
		this.clientSocket = clientSocket;
		this.processor = new RequestProcessor();
	}

	/**
	 * method called when a thread will execute this job reads the request from
	 * client socket converts the request in Request object in order to be
	 * processed by the processor serialize the response in order to be send
	 * writes the response back to client by opened socket close the socket
	 */
	@Override
	public Object call() throws Exception {
		Response response = null;
		try {
			Request request = readRequest();
			response = processor.processRequest(request);
		} catch (HttpException e) {
			response = processor.processHttpException(e);
		}
		sendResponse(response);
		closeConnection();
		return null;
	}

	/**
	 * read and parse first line of client http request
	 * read and parse headers 
	 * read body content using Content-Length header
	 */
	private Request readRequest() throws HttpException {
		Request request = new Request();
		try {
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			String line = in.readLine();
			HttpConverter.readFirstLine(line, request);

			StringBuilder builder = new StringBuilder();
			while (true) {
				line = in.readLine();
				builder.append(line);
				builder.append(NEW_LINE);
				if (null == line || line.isEmpty() || NEW_LINE.equals(line)) {
					break;
				}
			}
			HttpConverter.readHeaders(builder.toString(), request);

			builder = new StringBuilder();
			long bodyLength = HttpConverter.getBodyContentLength(request);
			for (int i = 0; i < bodyLength; i++) {
				char c = (char) in.read();
				builder.append(c);
			}
			request.setBody(builder.toString());

		} catch (IOException e) {
			LOG.error("Error while reading request " + e);
			return null;
		}
		return request;
	}

	/**
	 * send response to client
	 */
	private void sendResponse(Response response) {
		if (null == response) {
			return;
		}
		try {
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			out.println(HttpConverter.serializeHTTPResponse(response));
		} catch (IOException e) {
			LOG.error("Error while writing response ", e);
		}
	}

	/**
	 * close communication channels
	 */
	private void closeConnection() {
		try {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
			if (clientSocket != null) {
				clientSocket.close();
			}
		} catch (IOException e) {
			LOG.error("Failed to close connection " + e);
		}
	}
}
