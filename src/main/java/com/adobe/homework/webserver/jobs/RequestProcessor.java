package com.adobe.homework.webserver.jobs;

import static com.adobe.homework.webserver.utils.Constants.ACCEPTED_PROTOCOL;
import static com.adobe.homework.webserver.utils.Constants.PATH_SEPARATOR;
import static com.adobe.homework.webserver.utils.HttpStatuses.CREATED_CODE;
import static com.adobe.homework.webserver.utils.HttpStatuses.CREATED_STATUS;
import static com.adobe.homework.webserver.utils.HttpStatuses.OK_CODE;
import static com.adobe.homework.webserver.utils.HttpStatuses.OK_STATUS;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.InternalServerErrorException;

import org.apache.log4j.Logger;

import com.adobe.homework.properties.AppProperties;
import com.adobe.homework.webserver.entities.HttpHeader;
import com.adobe.homework.webserver.entities.Request;
import com.adobe.homework.webserver.entities.Response;
import com.adobe.homework.webserver.exceptions.ConflictException;
import com.adobe.homework.webserver.exceptions.ForbiddenException;
import com.adobe.homework.webserver.exceptions.HttpException;
import com.adobe.homework.webserver.exceptions.HttpFormatException;
import com.adobe.homework.webserver.exceptions.NotFoundException;
import com.adobe.homework.webserver.exceptions.NotImplementedException;

public class RequestProcessor {
	private static final Logger LOG = Logger.getLogger(RequestProcessor.class);

	/**
	 * process a request by HTTP method type
	 * 
	 * @param request
	 * @return response object
	 * @throws HttpException
	 */
	public Response processRequest(Request request) throws HttpException {
		if (null == request) {
			return null;
		}

		switch (request.getMethod()) {
		case GET:
			return processGet(request);
		case POST:
			return processPost(request);
		case PUT:
			return processPut(request);
		case DELETE:
			return processDelete(request);
		case CONNECT:
			return processConnect(request);
		case HEAD:
			return processHead(request);
		case OPTIONS:
			return processOptions(request);
		case TRACE:
			return processTrace(request);
		default:
			LOG.error("Invalid request " + request.getMethod());
			return null;
		}
	}

	/**
	 * construct an error response
	 * 
	 * @param exception
	 * @return HTTP response filled with exception information
	 */
	public Response processHttpException(HttpException e) {
		Response response = getEmptyResponse();
		response.setStatusCode(e.getStatusCode());
		response.setStatus(e.getStatus());
		response.setBody(e.getMessage());
		addHeaders(response);
		return response;
	}

	/**
	 * process HTTP GET request
	 * 
	 * @param request
	 * @return response
	 * 200 OK - read a local file resource
	 * 404 Not Found - the file doesn't exist or is hidden
	 * 403 Forbidden - user doesn't have rights to read the file
	 * @throws HttpException
	 */
	private Response processGet(Request request) throws HttpException {
		String url = request.getUrl();
		LOG.debug("Get resouce URL:" + url);

		String stringPath = getLocalPath(url);
		lockResource(stringPath);
		Path path = assertValidResource(stringPath);
		assertReadRights(path);

		Response response = getOKResponse();
		response.setBody(readContent(path));
		unlockResource(stringPath);
		addHeaders(response);
		return response;
	}


	/**
	 * process HTTP POST request
	 * 
	 * @param request
	 * @return response
	 * 201 Created - a new file resource is created
	 * 409 Conflict - the resource already exists
	 * @throws HttpException
	 */
	private Response processPost(Request request) throws HttpException {
		String url = request.getUrl();
		LOG.debug("Create resouce URL:" + url);

		String stringPath = getLocalPath(request.getUrl());
		lockResource(stringPath);
		assertInexistentResource(stringPath);

		Response response = getCreatedResponse();
		response.setBody(createResource(stringPath, request.getBody()));
		unlockResource(stringPath);
		addHeaders(response);
		return response;
	}


	/**
	 * process HTTP PUT request
	 * 
	 * @param request
	 * @return response
	 * 200 OK - the file resource content is override
	 * 404 Not Found - the file doesn't exist or is hidden
	 * 403 Forbidden - user doesn't have rights to write the file
	 * @throws HttpException
	 */
	private Response processPut(Request request) throws HttpException {
		String url = request.getUrl();
		LOG.debug("Update resouce URL:" + url);

		String stringPath = getLocalPath(url);
		lockResource(stringPath);
		Path path = assertValidResource(stringPath);
		assertWriteRights(path);

		Response response = getOKResponse();
		response.setBody(writeContent(path, request.getBody()));
		unlockResource(stringPath);
		addHeaders(response);
		return response;
	}


	/**
	 * process HTTP DELETE request
	 * 
	 * @param request
	 * @return response
	 * 200 OK - the file resource is deleted from local disk
	 * 404 Not Found - the file doesn't exist or is hidden
	 * 403 Forbidden - user doesn't have rights to write the file
	 * @throws HttpException
	 */
	private Response processDelete(Request request) throws HttpException {
		String url = request.getUrl();
		LOG.debug("Delete resouce URL:" + url);

		String stringPath = getLocalPath(url);
		lockResource(stringPath);
		Path path = assertValidResource(stringPath);
		assertWriteRights(path);

		Response response = getOKResponse();
		response.setBody(readContent(path));
		addHeaders(response);

		deleteResource(path);
		unlockResource(stringPath);
		return response;
	}


	/**
	 * process HTTP HEAD request
	 * 
	 * @param request
	 * @return response
	 * 200 OK - get headers of an existing resource
	 * 404 Not Found - the file doesn't exist or is hidden
	 * 403 Forbidden - user doesn't have rights to read the file
	 * @throws HttpException
	 */
	private Response processHead(Request request) throws HttpException {
		String url = request.getUrl();
		LOG.debug("Get Head resouce URL:" + url);

		String stringPath = getLocalPath(url);
		lockResource(stringPath);
		Path path = assertValidResource(stringPath);
		assertReadRights(path);

		Response response = getOKResponse();
		response.setBody(readContent(path));
		unlockResource(stringPath);
		addHeaders(response);
		response.setBody(null);
		return response;
	}

	/**
	 * Web server doesn't support HTTP CONNECT requests
	 */
	private Response processConnect(Request request) throws HttpException {
		throw new NotImplementedException("CONNECT method not implemented.");
	}

	/**
	 * Web server doesn't support HTTP OPTIONS requests
	 */
	private Response processOptions(Request request) throws HttpException {
		throw new NotImplementedException("OPTIONS method not implemented.");
	}

	/**
	 * Web server doesn't support HTTP TRACE requests
	 */
	private Response processTrace(Request request) throws HttpException {
		throw new NotImplementedException("TRACE method not implemented.");
	}

	/**
	 * verify if a requested resource exists
	 * 
	 * @param stringPath
	 * @return path
	 * @throws HttpException if file doesn't exist, is not a file or is hidden
	 */
	private Path assertValidResource(String stringPath) throws HttpException {
		try {
			Path path = Paths.get(stringPath);
			if (Files.notExists(path) || Files.isDirectory(path) || !Files.isRegularFile(path)
					|| Files.isHidden(path)) {
				throw new NotFoundException("Resource not found " + getRelativePath(stringPath));
			}
			return path;
		} catch (InvalidPathException e) {
			LOG.debug("Resource not found " + e);
			throw new NotFoundException("Resource not found " + getRelativePath(stringPath));
		} catch (IOException e) {
			LOG.debug("Resource not found " + e);
			throw new NotFoundException("Resource not found " + getRelativePath(stringPath));
		}
	}

	/**
	 * verify if a file resource doesn't exist in order to be creted
	 * 
	 * @param stringPath
	 * @throws HttpException if resource if found
	 */
	private void assertInexistentResource(String stringPath) throws HttpException {
		try {
			Path path = Paths.get(stringPath);
			if (Files.exists(path)) {
				throw new ConflictException("Resource already exists " + getRelativePath(stringPath));
			}
		} catch (InvalidPathException e) {
			return;
		}
	}

	/**
	 * verify if requested file resource has read rights
	 * 
	 * @param path
	 * @throws HttpException
	 */
	private void assertReadRights(Path path) throws HttpException {
		// TODO verify after authorization is done
		if (!Files.isReadable(path)) {
			throw new ForbiddenException("Not allowed to read file " + getRelativePath(path.toString()));
		}
	}


	/**
	 * verify if requested file resource has write rights
	 * 
	 * @param path
	 * @throws HttpException
	 */
	private void assertWriteRights(Path path) throws HttpException {
		// TODO verify after authorization is done
		if (!Files.isWritable(path)) {
			throw new ForbiddenException("Not allowed to write file " + getRelativePath(path.toString()));
		}
	}

	/**
	 * reads content of a requested resource file
	 * 
	 * @param path
	 * @return content of the file
	 * @throws HttpException
	 */
	private String readContent(Path path) throws HttpException {
		try {
			return new String(Files.readAllBytes(path));
		} catch (Exception e) {
			LOG.error("Failed to read file " + e);
			throw new InternalServerErrorException(e.getMessage());
		}
	}

	/**
	 * Writes content of an existing file
	 * 
	 * @param path
	 * @param body
	 * @return content of the file
	 * @throws HttpException
	 */
	private String writeContent(Path path, String body) throws HttpException {
		try {
			Files.write(path, body.getBytes(), StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
		} catch (Exception e) {
			LOG.error("Failed to write file " + e);
			throw new InternalServerErrorException(e.getMessage());
		}
		return body;
	}

	/**
	 * creates a new file
	 * created all the missing directories from given path
	 * 
	 * @param stringPath
	 * @param body
	 * @return
	 * @throws HttpException
	 */
	private String createResource(String stringPath, String body) throws HttpException {
		createDirectories(stringPath);
		return createFile(stringPath, body);
	}

	/**
	 * extracts the directories structure from a given path, removing last filename
	 * 
	 * @param stringPath
	 * @return directories structure
	 */
	private String extractDirectoriesPath(String stringPath) {
		String[] tokens = stringPath.split(PATH_SEPARATOR);
		StringBuilder directoriesPath = new StringBuilder();
		for (int i = 0; i < tokens.length - 1; i++) {
			directoriesPath.append(tokens[i]);
			directoriesPath.append(PATH_SEPARATOR);
		}
		return directoriesPath.toString();
	}

	/**
	 * created all the missing directory structure from given path
	 * 
	 * @param stringPath
	 */
	private void createDirectories(String stringPath) {
		String directoriesPath = extractDirectoriesPath(stringPath);
		if (null != directoriesPath && !directoriesPath.isEmpty()) {
			try {
				Path path = Paths.get(directoriesPath);
				if (Files.isDirectory(path)) {
					return;
				}
				Files.createDirectories(path);
			} catch (Exception e) {
				LOG.error("Failed to create directories " + e);
				throw new InternalServerErrorException(e.getMessage());
			}
		}
	}

	/**
	 * Creates a new file with given content file
	 * 
	 * @param stringPath
	 * @param body
	 * @return file content
	 * @throws HttpException
	 */
	private String createFile(String stringPath, String body) throws HttpException {
		try {
			Path path = Paths.get(stringPath);
			Files.write(path, body.getBytes(), StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
		} catch (Exception e) {
			LOG.error("Failed to create file " + e);
			throw new InternalServerErrorException(e.getMessage());
		}
		return body;
	}

	/**
	 * Delete an existing file
	 * 
	 * @param path
	 * @throws HttpException
	 */
	private void deleteResource(Path path) throws HttpException {
		try {
			Files.deleteIfExists(path);
		} catch (Exception e) {
			LOG.error("Failed to delete file " + e);
			throw new InternalServerErrorException(e.getMessage());
		}
	}

	/**
	 * Append application base path to a given resource path
	 * base path is configurable in application config file
	 * 
	 * @param stringURL
	 * @return local path
	 * @throws HttpFormatException
	 */
	private String getLocalPath(String stringURL) throws HttpFormatException {
		StringBuilder path = new StringBuilder();
		path.append(AppProperties.getBaseDirectory());
		path.append(stringURL);
		return path.toString();
	}

	/**
	 * Remove application base path in order not to be seen on client response
	 * 
	 * @param stringURL
	 * @return requested path
	 * @throws HttpFormatException
	 */
	private String getRelativePath(String stringURL) throws HttpFormatException {
		return stringURL.replaceFirst(AppProperties.getBaseDirectory(), "");
	}

	/**
	 * Construct an empty OK response object
	 */
	private Response getOKResponse() {
		Response response = getEmptyResponse();
		response.setStatusCode(OK_CODE);
		response.setStatus(OK_STATUS);
		return response;
	}

	/**
	 * Construct an empty Created response object
	 */
	private Response getCreatedResponse() {
		Response response = getEmptyResponse();
		response.setStatusCode(CREATED_CODE);
		response.setStatus(CREATED_STATUS);
		return response;
	}

	/**
	 * Construct an empty response object
	 */
	private Response getEmptyResponse() {
		Response response = new Response();
		response.setProtocol(ACCEPTED_PROTOCOL);
		return response;
	}

	/**
	 * Add headers to an existing response
	 * 
	 * @param response
	 */
	private void addHeaders(Response response) {
		Map<String, String> headers = new HashMap<>();
		if (response.getBody() != null) {
			String length = String.valueOf(response.getBody().length());
			headers.put(HttpHeader.CONTENT_LENGTH.header(), length);
		}
		response.setHeaders(headers);
	}

	/**
	 * Unlock mechanism that ensure other requests can be processed on given resource path
	 *  
	 * @param stringPath
	 */
	private void unlockResource(String stringPath) {
		// TODO Auto-generated method stub

	}

	/**
	 * Lock mechanism that ensure no other requests will be processed on given resource path
	 *  
	 * @param stringPath
	 */
	private void lockResource(String stringPath) {
		// TODO Auto-generated method stub

	}
}
