package com.adobe.homework.webserver.entities;

import org.apache.log4j.Logger;

public enum HttpMethod {
	GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE;

	private static final Logger LOG = Logger.getLogger(HttpMethod.class);

	public static HttpMethod getMethod(String method) {
		if (null == method) {
			return null;
		}
		try {
			return HttpMethod.valueOf(method.toUpperCase());
		} catch (IllegalArgumentException e) {
			LOG.error(String.format("Invalid HTTP method found: %s %s", method, e));
		}
		return null;
	}
}
