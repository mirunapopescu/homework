package com.adobe.homework.webserver.entities;

import java.util.Map;

public class Request {

	private HttpMethod method;
	private String url;
	private String protocol;
	private Map<String, String> headers;
	private Map<String, String> cookies;
	private String body;

	public HttpMethod getMethod() {
		return method;
	}

	public void setMethod(HttpMethod method) {
		this.method = method;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public Map<String, String> getCookies() {
		return cookies;
	}

	public void setCookies(Map<String, String> cookies) {
		this.cookies = cookies;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((body == null) ? 0 : body.hashCode());
		result = prime * result + ((cookies == null) ? 0 : cookies.hashCode());
		result = prime * result + ((headers == null) ? 0 : headers.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((protocol == null) ? 0 : protocol.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Request other = (Request) obj;
		if (body == null) {
			if (other.body != null) return false;
		} else if (!body.equals(other.body)) return false;
		if (cookies == null) {
			if (other.cookies != null) return false;
		} else if (!cookies.equals(other.cookies)) return false;
		if (headers == null) {
			if (other.headers != null) return false;
		} else if (!headers.equals(other.headers)) return false;
		if (method != other.method) return false;
		if (protocol == null) {
			if (other.protocol != null) return false;
		} else if (!protocol.equals(other.protocol)) return false;
		if (url == null) {
			if (other.url != null) return false;
		} else if (!url.equals(other.url)) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Request [method=" + method + ", url=" + url + ", protocol=" + protocol + ", headers=" + headers
				+ ", cookies=" + cookies + ", body=" + body + "]";
	}
}
