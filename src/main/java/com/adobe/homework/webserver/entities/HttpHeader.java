package com.adobe.homework.webserver.entities;

public enum HttpHeader {
	COOKIE("Cookie"), 
	SET_COOKIE("Set-Cookie"),
	ACCEPT("Accept"),
	CONNECTION("Connection"),
	CONTENT_TYPE("Content-Type"),
	CONTENT_LENGTH("Content-Length");
	
	private String header;
	
	private HttpHeader(String header) {
		this.header = header;
	}
	
	public String header() {
		return header;
	}
}
