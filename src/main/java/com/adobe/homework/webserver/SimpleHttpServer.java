package com.adobe.homework.webserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.adobe.homework.properties.AppProperties;
import com.adobe.homework.webserver.jobs.ProcessIOJob;

public class SimpleHttpServer implements Component {

	private static final Logger LOG = Logger.getLogger(SimpleHttpServer.class);
	private ServerSocket serverSocket;
	private ExecutorService executor;
	private boolean acceptNewRequests;

	/**
	 * starts the web server
	 */
	@Override
	public void Start() {
		createThreadPool();
		startListening();
	}

	/**
	 * stops the web server
	 */
	@Override
	public void Stop() {
		acceptNewRequests = false;
		shutdownThreadPool();
		stopListening();
	}

	/**
	 * creates a thread pool the maximum number of thread is fixed and is read
	 * from config file
	 */
	private void createThreadPool() {
		executor = Executors.newFixedThreadPool(AppProperties.getNoWorkers());
		LOG.info(String.format("ThreadPool created for maximum %s parallel workers.", AppProperties.getNoWorkers()));
	}

	/**
	 * shutdown the thread pool after all threads finish their jobs
	 */
	private void shutdownThreadPool() {
		executor.shutdown();
		while (!executor.isTerminated()) {
		}
		LOG.info("ThreadPool executor is down.");
	}

	/**
	 * opens a socket and starts listening for client requests listening port in
	 * read from config file for any client connection a new socket is created
	 * and passed to another thread from the pool
	 */
	private void startListening() {
		try {
			serverSocket = new ServerSocket(AppProperties.getServerPort());
		} catch (IOException e) {
			LOG.error("Failed to initialize socket " + e.toString());
		}
		acceptNewRequests = true;
		LOG.info(String.format("Web server started listening on port %s. ", AppProperties.getServerPort()));
		while (acceptNewRequests) {
			try {
				Socket clientSocket = serverSocket.accept();
				ProcessIOJob newRequest = new ProcessIOJob(clientSocket);
				executor.submit(newRequest);
			} catch (IOException e) {
				LOG.error("Failed to read message from client " + e.toString());
			}
		}
	}

	/**
	 * stops listening for client requests by closing the socket
	 */
	private void stopListening() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			LOG.error("Failed to close server socket.");
		}
		LOG.info(String.format("WebServer stoped listening on port %s.", AppProperties.getServerPort()));
	}
}
