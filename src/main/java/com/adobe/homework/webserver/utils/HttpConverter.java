package com.adobe.homework.webserver.utils;

import static com.adobe.homework.webserver.utils.Constants.COOKIE_SEPARATOR;
import static com.adobe.homework.webserver.utils.Constants.COOKIE_VALUE_SEPARATOR;
import static com.adobe.homework.webserver.utils.Constants.FIRST_LINE_SEPARATOR;
import static com.adobe.homework.webserver.utils.Constants.HEADER_SEPARATOR;
import static com.adobe.homework.webserver.utils.Constants.NEW_LINE;
import static com.adobe.homework.webserver.utils.HttpValidator.validateHeader;
import static com.adobe.homework.webserver.utils.HttpValidator.validateMethod;
import static com.adobe.homework.webserver.utils.HttpValidator.validateProtocol;
import static com.adobe.homework.webserver.utils.HttpValidator.validateURL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.adobe.homework.webserver.entities.HttpHeader;
import com.adobe.homework.webserver.entities.HttpMethod;
import com.adobe.homework.webserver.entities.Request;
import com.adobe.homework.webserver.entities.Response;
import com.adobe.homework.webserver.exceptions.HttpException;
import com.adobe.homework.webserver.exceptions.HttpFormatException;

public class HttpConverter {
	private static final Logger LOG = Logger.getLogger(HttpConverter.class);

	private HttpConverter() {
		// hide constructor
	}

	/**
	 * Parse first line of a HTTP request
	 * 
	 * @param stringRequest
	 * @param request
	 * @throws HttpException
	 * @throws IOException
	 */
	public static void readFirstLine(String stringRequest, Request request) throws HttpException, IOException {
		LOG.debug("Received first line: " + stringRequest);
		BufferedReader reader = new BufferedReader(new StringReader(stringRequest));
		try {
			String firstLine = reader.readLine();

			if (null == firstLine || firstLine.length() == 0) {
				throw new HttpFormatException("Invalid Request: " + firstLine);
			}

			String[] terms = firstLine.split(FIRST_LINE_SEPARATOR);
			if (terms.length != 3) {
				throw new HttpFormatException("Invalid Request: " + firstLine);
			}

			HttpMethod method = validateMethod(terms[0]);
			request.setMethod(method);

			validateURL(terms[1]);
			request.setUrl(terms[1]);

			validateProtocol(terms[2]);
			request.setProtocol(terms[2]);
		} catch (IOException e) {
			LOG.error("Failed to parse request ", e);
		}
	}

	/**
	 * Parse HTTP headers
	 * 
	 * @param stringRequest
	 * @param request
	 * @throws IOException
	 */
	public static void readHeaders(String stringRequest, Request request) throws IOException {
		LOG.debug("Received headers: " + stringRequest);
		BufferedReader reader = new BufferedReader(new StringReader(stringRequest));
		try {
			request.setHeaders(new HashMap<>());
			request.setCookies(new HashMap<>());
			String header = reader.readLine();
			while (header != null && header.length() > 0) {
				parseHeaderParameter(request, header);
				header = reader.readLine();
			}
		} catch (IOException e) {
			LOG.error("Failed to parse request ", e);
		}
	}

	/**
	 * Serialize a Response object
	 * 
	 * @param response
	 * @return response as String
	 */
	public static String serializeHTTPResponse(Response response) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(serializeFirstLine(response));
		buffer.append(serializeHeaders(response));
		buffer.append(NEW_LINE);
		buffer.append(response.getBody());
		return buffer.toString();
	}

	/**
	 * Get Content-Length header value from a request
	 * 
	 * @param request
	 * @return 
	 * Content-Length header value
	 * 0 if request doesn't contain Content-Length header
	 */
	public static long getBodyContentLength(Request request) {
		if (null == request || null == request.getHeaders()) {
			return 0;
		}
		String headerValue = request.getHeaders().get(HttpHeader.CONTENT_LENGTH.header());
		if (null == headerValue) {
			return 0;
		}
		try {
			return Long.valueOf(headerValue);
		} catch (NumberFormatException e) {
			LOG.error("invalid Content-Length:" + headerValue);
		}
		return 0;
	}

	/**
	 * Serialize HTTP response first line
	 */
	private static String serializeFirstLine(Response response) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(response.getProtocol());
		buffer.append(FIRST_LINE_SEPARATOR);
		buffer.append(response.getStatusCode());
		buffer.append(FIRST_LINE_SEPARATOR);
		buffer.append(response.getStatus());
		buffer.append(NEW_LINE);
		return buffer.toString();
	}

	/**
	 * Serialize HTTP response headers
	 */
	private static String serializeHeaders(Response response) {
		StringBuffer buffer = new StringBuffer();
		Map<String, String> headers = response.getHeaders();
		for (Map.Entry<String, String> entry : headers.entrySet()) {
			buffer.append(entry.getKey());
			buffer.append(HEADER_SEPARATOR);
			buffer.append(entry.getValue());
			buffer.append(NEW_LINE);
		}
		buffer.append(NEW_LINE);
		return buffer.toString();
	}

	/**
	 * Parse HTTP header and add it to request
	 * 
	 * @param request
	 * @param header
	 * @throws HttpException
	 */
	private static void parseHeaderParameter(Request request, String header) throws HttpException {
		int idx = header.indexOf(HEADER_SEPARATOR);
		if (idx == -1) {
			throw new HttpFormatException("Invalid Header Parameter: " + header);
		}

		String headerName = header.substring(0, idx).trim();
		String headerValue = header.substring(idx + 1, header.length()).trim();

		if (HttpHeader.COOKIE.header().equals(headerName)) {
			parseCookieParameter(request, headerValue);
			return;
		}

		validateHeader(headerName, headerValue);
		request.getHeaders().put(headerName, headerValue);
	}

	/**
	 * Parse HTTP cookie and add it to request
	 * 
	 * @param request
	 * @param stringCookies
	 * @throws HttpException
	 */
	private static void parseCookieParameter(Request request, String stringCookies) throws HttpException {
		String[] cookies = stringCookies.split(COOKIE_SEPARATOR);

		for (int i = 0; i < cookies.length; i++) {
			String cookie = cookies[i].trim();
			if (cookie.length() == 0) {
				continue;
			}

			int idx = cookie.indexOf(COOKIE_VALUE_SEPARATOR);
			if (idx == -1) {
				throw new HttpFormatException("Invalid Cookie Parameter: " + cookie);
			}

			String cookieName = cookie.substring(0, idx).trim();
			String cookieValue = cookie.substring(idx + 1, cookie.length()).trim();

			request.getCookies().put(cookieName, cookieValue);
		}
	}
}
