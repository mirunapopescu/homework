package com.adobe.homework.webserver.utils;

public class HttpStatuses {

	public static final Integer OK_CODE = 200;
	public static final Integer CREATED_CODE = 201;
	
	public static final String OK_STATUS = "OK";
	public static final String CREATED_STATUS = "Created";
}
