package com.adobe.homework.webserver.utils;

public class Constants {

	public static final String ACCEPTED_PROTOCOL = "HTTP/1.1";
	
	public static final String PATH_SEPARATOR = "/";
	public static final String FIRST_LINE_SEPARATOR = " ";
	public static final String HEADER_SEPARATOR = ":";
	public static final String COOKIE_SEPARATOR = ";";
	public static final String COOKIE_VALUE_SEPARATOR = "=";
	public static final String NEW_LINE = "\r\n";
}
