package com.adobe.homework.webserver.utils;

import static com.adobe.homework.webserver.utils.Constants.ACCEPTED_PROTOCOL;

import com.adobe.homework.webserver.entities.HttpMethod;
import com.adobe.homework.webserver.exceptions.HttpFormatException;
import com.adobe.homework.webserver.exceptions.NotSupportedException;

public class HttpValidator {

	/**
	 * verify if requested protocol is accepted
	 * 
	 * @param protocol
	 * @throws NotSupportedException
	 */
	public static void validateProtocol(String protocol) throws NotSupportedException {
		if (!ACCEPTED_PROTOCOL.equals(protocol)) {
			throw new NotSupportedException("HTTP Version not supported " + protocol);
		}
	}

	/**
	 * verify if requested method is a valid HTTP method
	 * 
	 * @param stringMethod
	 * @return
	 * @throws HttpFormatException
	 */
	public static HttpMethod validateMethod(String stringMethod) throws HttpFormatException {
		HttpMethod method = HttpMethod.getMethod(stringMethod);
		if (null == method) {
			throw new HttpFormatException("Invalid Http Method: " + stringMethod);
		}
		return method;
	}

	/**
	 * verify if stringURL is a valid URL
	 * 
	 * @param stringURL
	 * @throws HttpFormatException
	 */
	public static void validateURL(String stringURL) throws HttpFormatException {
		// TODO
	}

	/**
	 * verify if header is valid
	 * 
	 * @param headerName
	 * @param headerValue
	 */
	public static void validateHeader(String headerName, String headerValue) {
		// TODO
	}
}
