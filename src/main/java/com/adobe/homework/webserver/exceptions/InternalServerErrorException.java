package com.adobe.homework.webserver.exceptions;

import javax.ws.rs.core.Response;

public class InternalServerErrorException extends HttpException {
	private static final long serialVersionUID = 1551276417686440290L;

	public InternalServerErrorException() {
		super(Response.Status.INTERNAL_SERVER_ERROR);
	}

	public InternalServerErrorException(String message) {
		super(Response.Status.INTERNAL_SERVER_ERROR, message);
	}
}
