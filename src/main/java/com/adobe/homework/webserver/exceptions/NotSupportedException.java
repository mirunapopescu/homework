package com.adobe.homework.webserver.exceptions;

import javax.ws.rs.core.Response;

public class NotSupportedException extends HttpException {
	private static final long serialVersionUID = 1551276417686440290L;

	public NotSupportedException() {
		super(Response.Status.HTTP_VERSION_NOT_SUPPORTED);
	}

	public NotSupportedException(String message) {
		super(Response.Status.HTTP_VERSION_NOT_SUPPORTED, message);
	}
}
