package com.adobe.homework.webserver.exceptions;

import javax.ws.rs.core.Response;

public class NotImplementedException extends HttpException {
	private static final long serialVersionUID = 1551276417686440290L;

	public NotImplementedException() {
		super(Response.Status.NOT_IMPLEMENTED);
	}

	public NotImplementedException(String message) {
		super(Response.Status.NOT_IMPLEMENTED, message);
	}
}
