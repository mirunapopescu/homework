package com.adobe.homework.webserver.exceptions;

import javax.ws.rs.core.Response;

public class ForbiddenException extends HttpException {
	private static final long serialVersionUID = 1551276417686440290L;

	public ForbiddenException() {
		super(Response.Status.FORBIDDEN);
	}

	public ForbiddenException(String message) {
		super(Response.Status.FORBIDDEN, message);
	}
}
