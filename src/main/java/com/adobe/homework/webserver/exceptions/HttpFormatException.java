package com.adobe.homework.webserver.exceptions;

import javax.ws.rs.core.Response;

public class HttpFormatException extends HttpException {
	private static final long serialVersionUID = 5374515103657203990L;

	public HttpFormatException() {
		super(Response.Status.BAD_REQUEST);
	}

	public HttpFormatException(String message) {
		super(Response.Status.BAD_REQUEST, message);
	}
}
