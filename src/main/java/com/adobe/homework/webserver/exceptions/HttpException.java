package com.adobe.homework.webserver.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

public class HttpException extends WebApplicationException {
	private static final long serialVersionUID = 5374515103657203990L;
	private String message;

	public HttpException(Status status) {
		this(status, null);
	}

	public HttpException(Status status, String message) {
		super(status);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public String getStatus() {
		return getResponse().getStatusInfo().toString();
	}

	public Integer getStatusCode() {
		return getResponse().getStatus();
	}
}
