package com.adobe.homework.webserver.exceptions;

import javax.ws.rs.core.Response;

public class NotFoundException extends HttpException {
	private static final long serialVersionUID = 1551276417686440290L;

	public NotFoundException() {
		super(Response.Status.NOT_FOUND);
	}

	public NotFoundException(String message) {
		super(Response.Status.NOT_FOUND, message);
	}
}
