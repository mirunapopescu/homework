package com.adobe.homework.webserver.exceptions;

import javax.ws.rs.core.Response;

public class ConflictException extends HttpException {
	private static final long serialVersionUID = 1551276417686440290L;

	public ConflictException() {
		super(Response.Status.CONFLICT);
	}

	public ConflictException(String message) {
		super(Response.Status.CONFLICT, message);
	}
}
