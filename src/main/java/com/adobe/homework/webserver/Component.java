package com.adobe.homework.webserver;

public interface Component {
	public void Start();
	public void Stop();
}
