package com.adobe.homework.webserver.utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ResourceReader {

	public static String readFrom(String resourceName) {
		try {
			URL url = Thread.currentThread().getContextClassLoader().getResource(resourceName);
			if (null == url) {
				throw new RuntimeException("Resource " + resourceName + " not found");
			}
			Path path = Paths.get(Thread.currentThread().getContextClassLoader().getResource(resourceName).toURI());
			return new String(Files.readAllBytes(path));
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
