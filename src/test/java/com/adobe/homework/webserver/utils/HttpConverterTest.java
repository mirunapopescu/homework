package com.adobe.homework.webserver.utils;

import static com.adobe.homework.webserver.utils.ResourceReader.readFrom;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.adobe.homework.webserver.entities.HttpHeader;
import com.adobe.homework.webserver.entities.HttpMethod;
import com.adobe.homework.webserver.entities.Request;
import com.adobe.homework.webserver.exceptions.HttpFormatException;
import com.adobe.homework.webserver.exceptions.NotSupportedException;

public class HttpConverterTest {

	private static final String TESTCASES_PATH = "testcases/converter/";

	@Test
	public void parseValidHTTPGetRequestTest() {

		String firstline = readFrom(TESTCASES_PATH + "valid_get_firstline");
		String headersLine = readFrom(TESTCASES_PATH + "valid_headers");

		Request expected = new Request();
		expected.setMethod(HttpMethod.GET);
		expected.setProtocol("HTTP/1.1");
		expected.setUrl("/path/filename");
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeader.ACCEPT.header(), "text/plain");
		headers.put(HttpHeader.CONNECTION.header(), "Keep-Alive");
		expected.setHeaders(headers);
		Map<String, String> cookies = new HashMap<>();
		cookies.put("Auth", "123456");
		cookies.put("Auth2", "altceva");
		expected.setCookies(cookies);

		try {
			Request actualResult = new Request();
			HttpConverter.readFirstLine(firstline, actualResult);
			HttpConverter.readHeaders(headersLine, actualResult);
			Assert.assertEquals(expected, actualResult);
		} catch (Exception e) {
			fail();
		}
	}

	@Test(expected = HttpFormatException.class)
	public void parseInvalidHTTPPostRequestNoUrlTest() throws HttpFormatException {

		String stringRequest = readFrom(TESTCASES_PATH + "invalid_post_no_url");

		String expectedMessage = "Invalid Request: POST HTTP/1.1";
		String expectedStatus = "Bad Request";
		Integer expectedStatusCode = 400;

		try {
			HttpConverter.readFirstLine(stringRequest, new Request());
		} catch (HttpFormatException e) {
			Assert.assertEquals(expectedMessage, e.getMessage());
			Assert.assertEquals(expectedStatus, e.getStatus());
			Assert.assertEquals(expectedStatusCode, e.getStatusCode());
			throw e;
		} catch (Exception e) {
			fail();
		}
	}

	@Test(expected = NotSupportedException.class)
	public void parseInvalidHTTPPostRequestProtocolTest() throws NotSupportedException {

		String stringRequest = readFrom(TESTCASES_PATH + "invalid_post_protocol");

		String expectedMessage = "HTTP Version not supported HTTP/1.10";
		String expectedStatus = "HTTP Version Not Supported";
		Integer expectedStatusCode = 505;

		try {
			HttpConverter.readFirstLine(stringRequest, new Request());
		} catch (NotSupportedException e) {
			Assert.assertEquals(expectedMessage, e.getMessage());
			Assert.assertEquals(expectedStatus, e.getStatus());
			Assert.assertEquals(expectedStatusCode, e.getStatusCode());
			throw e;
		} catch (Exception e) {
			fail();
		}
	}
}
